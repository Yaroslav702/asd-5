'use strict';
export default class Graph {
  constructor(numberNodes) {
    this.numberNodes = numberNodes;
    this.adjacencyMatrix = [];
    this.isDirected = true;
    for (let i = 0; i < this.numberNodes; i++) {
      this.adjacencyMatrix[i] = new Array(this.numberNodes).fill(0);
    }
  }
  randm(generator) {
    for (const arr of this.adjacencyMatrix) {
      for (let i = 0; i < arr.length; i++) {
        arr[i] = generator() * 2;
      }
    }
  }
  mulmr(k) {
    for (const arr of this.adjacencyMatrix) {
      for (let i = 0; i < arr.length; i++) {
        arr[i] = arr[i] * k >= 1 ? 1 : 0;
      }
    }
  }
  makeEdgesList() {
    const matrix = this.adjacencyMatrix;
    const adjList = [];
    for (let i = 0; i < matrix.length; i++) {
      adjList[i] = [];
    }
    for (let i = 0; i < matrix.length; i++) {
      for (let j = 0; j < matrix.length; j++) {
        if (matrix[i][j] === 1) {
          adjList[i].push(j);
        }
      }
    }
    return adjList;
  }
  isPath(start, finish, adj) {
    const stack = [];
    stack.push(start);
    const visited = new Array(adj.length + 1).fill(0);
    let current = start;
    while (stack.length) {
      if (current === finish) {
        return true;
      }
      stack.pop();
      for (const el of adj[current]) {
        if (!visited[el]) {
          stack.push(el);
        }
      }
      visited[current] = true;
      current = stack.at(-1);
    }
    return false;
  }
  findComponents() {
    const n = this.adjacencyMatrix.length;
    const ans = [];
    const isScc = new Array(n + 1).fill(0);
    const adjList = this.makeEdgesList();
    for (let i = 0; i < n; i++) {
      if (!isScc[i]) {
        const scc = [i];
        for (let j = i + 1; j < n; j++) {
          if (!isScc[j] && this.isPath(i, j, adjList) && this.isPath(j, i, adjList)) {
            isScc[j] = 1;
            scc.push(j);
          }
        }
        ans.push(scc);
      }
    }
    return ans;
  }
  calcBfs(begin) {
    const edges = this.makeEdgesList();

    const visited = new Array(edges.length).fill(false);

    const res = [];

    const queue = [];
    queue.unshift(begin);

    visited[begin] = true;

    for (let i = 0; i < queue.length; i++) {
        const current = queue[i];
        for (const el of edges[current]) {
            if (!visited[el]) {
                queue.push(el);
                res.push([current, el]);
                visited[el] = true;
            }
        }
    }
    return res;
  }
  calcDfs(begin) {
    const edges = this.makeEdgesList();

    const visited = new Array(edges.length).fill(false);

    const res = [];
    const stack = [begin];

    for (let current = stack.pop(); current !== undefined; current = stack.pop()) {
        if (!visited[current]) {
            visited[current] = true;
            res.push(current);
            for (const el of edges[current].reverse()) {
                if (!visited[el]) {
                    stack.push(el);
                }
            }
        }
    }
    return res;
  }

  bfsWrapper(pathOfEdges, bfs, pathMatrix) {
    const components = this.findComponents();

    for (let i = 0; i < components.length; i++) {
      for (const el of this.calcBfs(components[i][0])) {
        if (!pathOfEdges.includes(el)) {
          pathOfEdges.push(el);
        }
      }
      for (const [from, to] of pathOfEdges) {
        if (!bfs.includes(from)) {
          bfs.push(from);

          pathMatrix[from][to] = 1;
        }
        if (!bfs.includes(to)) {
          bfs.push(to);

          pathMatrix[from][to] = 1;
        }
      }
    }
  }
  dfsWrapper(pathOfEdges, dfs, pathMatrix) {
    const edges = this.makeEdgesList();
    const components = this.findComponents();

    for (let i = 0; i < components.length; i++) {
      for (const el of this.calcDfs(components[i][0])) {
        if (!dfs.includes(el)) {
          dfs.push(el);
        }
      }
    }
    for (let i = 1; i < dfs.length; i++) {
      let from = dfs[i - 1];
      let to = dfs[i];
      let k = i - 1;
      while (!edges[from].includes(to)) {
        k--;
        if (k < 0) break;
        from = dfs[k];
      }
      if (k >= 0) {
        pathOfEdges.push([from, to]);

        pathMatrix[from][to] = 1;
      }
    }
  }
}
