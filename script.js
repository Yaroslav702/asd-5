'use strict';

import Graph from './graph.js';
import GraphDraw from './graphDraw.js';

const n1 = 3;
const n2 = 2;
const n3 = 0;
const n4 = 5;
let k = 1.0 - n3 * 0.01 - n4 * 0.005 - 0.15;

const canvas = document.getElementById('canvas');
const context = canvas.getContext('2d');

canvas.height = window.innerHeight;
canvas.width = window.innerWidth;

const nodeRadius = 30;
const distanceFromCenter = 350;
const amountOfNodes = 10 + n3;
const generator = new Math.seedrandom([n1, n2, n3, n4].join);
let graph = new Graph(amountOfNodes);
let path = [];
let graphDraw;
let counter = 1;
let pathOfEdges = [];
let pathMatrix = new Array(amountOfNodes);
for (let i = 0; i < amountOfNodes; i++) {
  pathMatrix[i] = new Array(amountOfNodes).fill(0);
}

graph.randm(generator);
graph.mulmr(k);
graphDraw = new GraphDraw(canvas, graph, nodeRadius);
graphDraw.graphDrawTriangle(distanceFromCenter);
console.log(graph.adjacencyMatrix);

document.getElementById('reset').addEventListener('click', () => {
  console.clear();
  context.clearRect(0, 0, canvas.width, canvas.height);
  path = [];
  pathOfEdges = [];
  pathMatrix = new Array(amountOfNodes);
  for (let i = 0; i < amountOfNodes; i++) {
    pathMatrix[i] = new Array(amountOfNodes).fill(0);
  }
  graphDraw.graphDrawTriangle(distanceFromCenter);
  console.log(graph.adjacencyMatrix);
});

document.getElementById('bfs').addEventListener('click', () => {
  graph.bfsWrapper(pathOfEdges, path, pathMatrix);
  counter = 1;
  console.log(pathMatrix);
  console.log(path);
});
document.getElementById('dfs').addEventListener('click', () => {
  graph.dfsWrapper(pathOfEdges, path, pathMatrix);
  counter = 1;
  console.log(pathMatrix);
  console.log(path);
});
document.getElementById('next-step').addEventListener('click', () => {
  if (counter < amountOfNodes) {
    const [from, to] = pathOfEdges[counter - 1];
    graphDraw.drawPath(from, to);
    counter++;
  }
});
